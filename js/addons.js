$('#file-input').on('change',function(){
    //get the file name
    // var fileName = $(this).val();
    //replace the "Choose a file" label
    const contentFile = $(this).val().split("\\");
    const name = contentFile[contentFile.length-1];
    // console.log(name);
    $(this).next('.custom-file-label').html(name);
    // $("#btn-submit").prop("disabled", this.files.length == 0);// habilitar o deshabilitar el boton
    
    console.log(this.files.length);
    if(this.files.length == 0){
        // $("#btn-submit").prop("disabled",true);
        // $("#btn-submit").removeClass('btn-outline-success');
        // $("#btn-submit").removeClass('btn-outline-danger');
        // $("#btn-submit").addClass('btn-outline-secondary');
        
        // $(this).next('.custom-file-label').html('Seleccionar archivo...');
        whitoutFile();
    }else{
        const file = $('#file-input')[0].files[0];
        const content = file.name.split('.');
        const extInput = content[content.length-1];
        if(validations(extInput)){
            done();
        }else{
           fail();
        }
    }
})

function done() {
    $("#btn-submit").removeClass('btn-outline-secondary');
    $("#btn-submit").removeClass('btn-outline-danger');
    $("#btn-submit").addClass('btn-outline-success');
    $("#btn-submit").prop("disabled",false);
}

function fail() {
    $("#btn-submit").removeClass('btn-outline-secondary');
    $("#btn-submit").removeClass('btn-outline-success');
    $("#btn-submit").addClass('btn-outline-danger');
    $("#btn-submit").prop("disabled",true);
}
