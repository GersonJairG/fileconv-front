$('#btn-submit').on('click', (e) => {    
    e.preventDefault();    
    const form = $('#form-upload')[0];
    const data = new FormData(form);    
    console.log(data);
    const file = $('#file-input')[0].files[0];
    const contentFile = file.name.split('.');
    const extInput = contentFile[contentFile.length-1];
    if(validations(extInput)){        
        uploading(data);          
    }else{
        alert('Extension no valida');
    }
})

$('#btn-download').on('click', () => {
    $('#loader').css('display','block');
    $('#download-text').css('display','none');
})

$('#btn-download').on('click', (e) => {
    e.preventDefault();    
    const file = $('#file-input')[0].files[0];

    const contentFile = file.name.split('.');
    const extInput = contentFile[contentFile.length-1];
    const name = file.name.split('.'+extInput)[0];
    console.log('Nombre: ' + name);
    console.log('Extension: ' + extInput);    

    downloading(name,extInput);
})


function uploading (data) {
    converterModule.upload(data)
    .then((response) => {        
        console.log("SUCCESS : ", response.response);        
        $('#btn-download').css('display','block');
        $('#alert-final').css('display','none');
        $("#btn-submit").prop("disabled",true);
    }).catch((error)=> {        
        console.log("ERROR : ", error);
        // $("#btn-submit").prop("disabled", false);
    })
}


function downloading (name, extInput) {
    const extOutput = getOutput(extInput);
    if(extOutput === '')
        alert('Extension no valida')
    else
    converterModule.download(name, extInput, extOutput)
    .then((response) => {
        console.log("SUCCESS : ", response);
        $('#btn-download').css('display','none');
        $('#loader').css('display','none');
        $('#download-text').css('display','block');
        $('#alert-final').css('display','block');

        whitoutFile();
        var a = document.createElement('a');
        var url = window.URL.createObjectURL(response);
        a.href = url;
        a.download = name+'.'+extOutput;
        a.click();
        window.URL.revokeObjectURL(url);        
    }).catch((error)=> {
        console.log("ERROR : ", error);
    })
}

function getOutput (extInput) {
    if(extInput.includes('doc'))
        return 'odt'
    else if(extInput.includes('xls'))
        return 'ods'
    else if(extInput.includes('ppt'))
        return 'odp'
    else if(extInput.includes('odt'))
        return 'docx'
    else if(extInput.includes('ods'))
        return 'xlsx'
    else if(extInput.includes('odp'))
        return 'pptx'
    return '';
}

function validations (extInput) {
    if(extInput.includes('doc') || extInput.includes('odt') || 
    extInput.includes('xls') || extInput.includes('ods') || 
    extInput.includes('ppt') || extInput.includes('odp')){
        return true;
    }
    return false;
}

function whitoutFile(){
    $("#btn-submit").prop("disabled",true);
    $("#btn-submit").removeClass('btn-outline-success');
    $("#btn-submit").removeClass('btn-outline-danger');
    $("#btn-submit").addClass('btn-outline-secondary');
    
    $('#file-input').next('.custom-file-label').html('Seleccionar archivo...');
}