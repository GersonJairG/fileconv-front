const converterModule = {
    upload: function(data) {        
        return $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',//para poder enviar archivos
            url: "http://localhost:3000/up",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000
        });
    },
    download: function(name, extInput, extOutput) {
        console.log("En controller");
        return $.ajax({
            url: 'http://localhost:3000/down',
            method: 'post',
            data: {
                name: name,
                extInput: extInput,
                extOutput: extOutput
            },
            xhrFields: {
                responseType: 'blob'
            }
        });
    }
}